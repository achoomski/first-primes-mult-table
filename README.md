print_primes_table.rb will print a multiplication table of the first 10 primes. Alternatively, you can invoke it with a parameter specifying which value of N to use.

The code is split into a module called 'FirstPrimes', 'MultiplicationTable', and 'PrintPrimesTable.rb' (acts as main).

We first compute the first N primes using the Sieve of Eratosthenes combined with a function for computing an upper bound for the 
first N primes. Then we iterate over the rows and columns and output the cell values. This is done using a streaming implementation
to avoid storing the entire multiplication table in memory.

Time Complexity: O(n^2) to compute the multiplication table. Calculating the primes takes time O(nlogn * loglog(nlogn)) which is less than O(n^2).
Memory Complexity: O(nlogn)


 


