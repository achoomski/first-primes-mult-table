class MultiplicationTable
  def initialize(values)
    @values = values
    @row = 0
    @col = 0
  end

  #acts like a stream to reduce memory usage (as opposed to storing the entire multiplication table in memory)
  def next
    if @done
      return nil
    end

    if @col == @values.length + 1
      advance
      return "\n"
    end

    if @row == 0 && @col == 0
      result = "X"
      advance
      return result
    end

    if @row == 0 || @col == 0
      result = @values[@row + @col - 1]
      advance
      return result
    end

    #neither are zero
    result = @values[@row - 1] * @values[@col - 1]
    advance
    return result
  end

  def advance
    #the last column in the table is a newline
    if @col < @values.length + 1
      @col+=1
    else
      if @row < @values.length
        @col = 0
        @row +=1
      else
        #we're at the last col and row
        @col, @row = nil, nil
        @done = true
      end
    end
  end


  end