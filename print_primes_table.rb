require_relative "first_primes"
require_relative "multiplication_table"

if ARGV.size == 0
  primes = FirstPrimes.first_n_primes
else
  primes = FirstPrimes.first_n_primes ARGV[0].to_i
end

mt = MultiplicationTable.new(primes)
n = mt.next
while n != nil
  str = n == "\n" ? "#{n}" : "#{n} "
  print str
  n = mt.next
end

