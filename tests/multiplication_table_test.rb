require_relative "../multiplication_table"
require "test/unit"

class MultiplicationTableTest < Test::Unit::TestCase
  def test_zero_length
    mt = MultiplicationTable.new([])
    assert_equal('X', mt.next)
    assert_equal("\n", mt.next)
    assert_equal(nil, mt.next)
  end

  def test_simple
    mt = MultiplicationTable.new([2, 3, 4])
    assert_equal("X", mt.next)
    assert_equal(2, mt.next)
    assert_equal(3, mt.next)
    assert_equal(4, mt.next)
    assert_equal("\n", mt.next)
    assert_equal(2, mt.next)
    assert_equal(4, mt.next)
    assert_equal(6, mt.next)
    assert_equal(8, mt.next)
    assert_equal("\n", mt.next)
    assert_equal(3, mt.next)
    assert_equal(6, mt.next)
    assert_equal(9, mt.next)
    assert_equal(12, mt.next)
    assert_equal("\n", mt.next)
    assert_equal(4, mt.next)
    assert_equal(8, mt.next)
    assert_equal(12, mt.next)
    assert_equal(16, mt.next)
    assert_equal("\n", mt.next)
    assert_equal(nil, mt.next)
    assert_equal(nil, mt.next)
  end
end
