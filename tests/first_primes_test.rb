require_relative "../first_primes"
require "test/unit"

class FirstPrimesTest < Test::Unit::TestCase
  def test_valid_small
  assert_equal([2], FirstPrimes.first_n_primes(1))
    assert_equal([2, 3], FirstPrimes.first_n_primes(2))
    assert_equal([2, 3, 5], FirstPrimes.first_n_primes(3))
    assert_equal([2,3,5,7,11,13,17,19,23,29], FirstPrimes.first_n_primes(10))
    assert_equal([2,3,5,7,11,13,17,19,23,29,
                  31,37,41,43,47,53,59,61,67,71], FirstPrimes.first_n_primes(20))
    assert_equal([2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,
     53,59,61,67,71,73,79,83,89,97,101,103,107,
     109,113,127,131,137,139,149,151,157,163,167,173,
     179,181,191,193,197,199,211,223,227,229,
     233,239,241,251,257,263,269,271,277,281,
     283,293,307,311,313,317,331,337,347,349,
     353,359,367,373,379,383,389,397,401,409,
     419,421,431,433,439,443,449,457,461,463,
     467,479,487,491,499,503,509,521,523,541], FirstPrimes.first_n_primes(100))
  end

  def test_valid_large
    primes = FirstPrimes.first_n_primes(1000)
    assert_equal(1000, primes.count)
    assert_equal(primes[-1], 7919)

    primes = FirstPrimes.first_n_primes(1000000)
    assert_equal(1000000, primes.count)
    assert_equal(primes[-1], 15485863)
  end

  def test_invalid_n
    exception = assert_raise(ArgumentError) do
      FirstPrimes.first_n_primes(0)
    end
    assert_equal("Invalid argument to FirstPrimes.first_n_primes. N should be a positive integer", exception.message)

    exception = assert_raise(ArgumentError) do
      FirstPrimes.first_n_primes(-1)
    end
    assert_equal("Invalid argument to FirstPrimes.first_n_primes. N should be a positive integer", exception.message)
  end

  def test_upper_bound_for_first_n_primes
    assert_operator(FirstPrimes.upper_bound_for_first_n_primes(1), :>= , 2)
    assert_operator(FirstPrimes.upper_bound_for_first_n_primes(2), :>= , 3)
    assert_operator(FirstPrimes.upper_bound_for_first_n_primes(100), :>= , 541)
    assert_operator(FirstPrimes.upper_bound_for_first_n_primes(2000000), :>= , 32452843)
  end

end
