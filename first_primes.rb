module FirstPrimes

  def self.first_n_primes(n=10)
    if n < 1
      raise ArgumentError.new("Invalid argument to FirstPrimes.first_n_primes. N should be a positive integer")
    end

    upper_bound = self.upper_bound_for_first_n_primes(n)

    potential_primes = Array.new(upper_bound+1, true)
    potential_primes[0], potential_primes[1] = false, false

    potential_primes.each_with_index do |is_prime, prime_number|
      next if !is_prime
      (prime_number**2..potential_primes.count).step(prime_number).each do |unprime_number|
        potential_primes[unprime_number] = false
      end
    end

    primes, i = [], 0
    while primes.count < n && i <= upper_bound
      if potential_primes[i] == true
        primes.push i
      end
      i+=1
    end

    return primes
  end

  def self.upper_bound_for_first_n_primes(n)
    if n < 1
      raise ArgumentError.new("Invalid argument to FirstPrimes.upper_bound_for_first_n_primes. N should be a positive integer")
    end
    #upper bound for nth prime. there is potential for optimization by
    #using tighter bounds based on n.http://math.univ-lille1.fr/~ramare/TME-EMT/Articles/Art01.html
    if n == 1
      return 2
    end
    return (n * (Math.log(n) + Math.log(Math.log(n)) + 8)).ceil
  end
end